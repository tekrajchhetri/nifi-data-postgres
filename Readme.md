# Inserting form data into database using Spring
------
The template and driver here is a part of blog post (http://blog.tekrajchhetri.com/inserting-data-into-postgresql-using-apache-nifi/) describing how can you insert data into database using nifi.

## Tools 
* Apache Nifi 
* Java - Since Apache nifi requires it
* PostgreSQL - As we are using it to insert data.


## Running
* Go to bin directory of nifi
* Issue ./nifi.sh start and open localhost:8080/nifi in web browser
